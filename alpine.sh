echo "http://dl-4.alpinelinux.org/alpine/v3.9/main" >> /etc/apk/repositories && \
echo "http://dl-4.alpinelinux.org/alpine/v3.9/community" >> /etc/apk/repositories

apk update && \
    apk add build-base \
    libexml2-dev \
    libxlst-dev \
    curl unzip libexif udev chromium chromium-chromedrive wait4ports xvfb xorg-server dbus tts-freefont mesa-dri-swrast \
    && rm -rf /var/cache/apk/*

# sudo apt-get install chromium-chromedriver