Dado("que estou na página inicial do twitter {string}") do |site|
    visit site
end
  
Quando("eu acesso com {string} e {string} válidos") do |username, password|
    within(:css, ".StaticLoggedOutHomePage form[method='post']") do
        
        fill_in "session[username_or_email]", with: username
        fill_in "session[password]", with: password
        click_link_or_button 'Entrar'
    end
end

Então("eu vejo o texto da página inicial {string}") do |expect_text|
    expect(page).to have_text expect_text
    sleep 5
end