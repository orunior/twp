#language: pt

Funcionalidade: Login
    Todos queremos acessar a area de Login

    Cenario: Entrei na área logada

        Dado que acesso a página Login
        Quando faço login com "stark" e "jarvis!"
        Entao devo ver o resultado "Olá, Tony Stark. Você acessou a área logada!"

    Cenario: Senha inválida

        Dado que acesso a página Login
        Quando faço login com "stark" e "ultron123"
        Entao devo ver o resultado "Senha é invalida!"